package com.example.acronymtest;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final EditText textField;
		final Button storeAcronym;
		final TextView displayAcronym;

		textField = (EditText) findViewById(R.id.inputPhrase);
		storeAcronym = (Button) findViewById(R.id.storeAcronym);
		displayAcronym = (TextView) findViewById(R.id.displayAcronym);

		storeAcronym.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String text = textField.getText().toString();

				displayAcronym.setText(text);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
